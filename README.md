# Quadogeo
Répertoire d'outils associé au groupe [QuaDoGeo du CNIG](http://cnig.gouv.fr/?page_id=18183)

## Quadorender (POC)
[Quadorender, preuve de concept d'une restitution automatisée de la qualité](QUADORENDER.md)

## QGIS
Méthodes de qualification de la donnée géographique sous QGIS, selon ISO-19157 (en cours)

## Ressources et présentations
[Ressources et présentations](RESSOURCES.md)

## Fiches quadogeo
https://github.com/CEREMA/quadogeo/tree/master/fiches/fiches-iso-19157
